# Using GitLab for Modern Application Development

Welcome to the Using GitLab for Modern Application Development Workshop. Congratulations on getting logged in to you pre-provisioned account on our [workshop GitLab instance](http://gitlab.tanuki.host!

## Introduction
<a name="introduction"></a>

GitLab provides a single tool for the entire DevOps software delivery lifecycle, right out of the box, freeing teams from toolchain
maintenance and allowing them to put all their energy into innovating. In this workshop you will get a hands-on experience of how
GitLab can help you create your application, build it, test it, secure it, package it, release it, and monitor it.

Kubernetes simplifies the management and use of containers as a way to deploy cloud native application loads. Kubernetes makes
spinning up, scaling up and down, and retiring containers easier than it has ever been. Kubernetes is helping to bring the use of
modern day software development and delivery practices to a larger number of practitioners.

However, a Kubernetes cluster alone doesn’t make great apps. Developers do. The software that is running in these clusters needs
to be imagined, created, tested, released, and managed. GitLab is a single application that provides all of these capabilities
out of the box, for a seamless, low maintenance, just-commit-code software development, and delivery experience.

### Learning Objectives

* Import a web app project and configure it for CI/CD
* Use GitLab Issues and MR's to plan a change
* Make some simple code changes
* Explore a GitLab Auto DevOps pipeline as it builds, tests, secures, packages, and deploys your app
* Review results in Merge Requests and Review Apps
* Deploy changes to production
* See how an out-of-the-box DevOps software delivery pipeline can help you focus on creating great software, instead of wrestling with integrations and maintenance of your toolchain
* Observe how GitLab combines Kubernetes and Continuous Delivery to provide an a single solution for the entire DevOps life cycle
* Discover how GitLab and Kubernetes are tightly integrated together so you can leverage the power of Kubernetes, but without having to manage it yourself


### Before We Begin
This workshop is focused on using GitLab for software development and delivery. As such, it will not focus on the setup and configuration
of the GitLab instance itself. Instead, a workshop account has been provided for you on this existing GitLab instance. If you would like to learn more about setting up your own GitLab instance there are other workshops which cover setting up a GitLab
instance on a hosted GKE cluster. You can alternatively subscribe to our [GitLab.com SaaS offering](https://gitlab.com/users/sign_in).

We will be doing pretty much all of our work in the GitLab UI, including viewing and editing files. All you need is a laptop with a recent version web browser installed (we recommend [Google Chrome web browser](https://www.google.com/chrome/)).

You will notice that your account has exactly one project in it named **gitlab-for-modern-app-dev**. This is the project that we will work from for the duration of the workshop.

**NOTE:** It is highly recommended to keep one tab/window open with the instructions, and another tab/window open to work from.


### Getting Started

These labs are self-paced, but they need to be done in the order that they have been written. When you finish a lab feel free to move to the next one.

Please proceed ahead to [Lab 1 - Setup Your Project and CI/CD](lab1/README.md).

* [Lab 1 - Setup your project and CI/CD](lab1/README.md)
* [Lab 2 - Capture and track your work](lab2/README.md)
* [Lab 3 - Make some code changes](lab3/README.md)
* [Lab 4 - Review pipeline results and deploy to production](lab4/README.md)
